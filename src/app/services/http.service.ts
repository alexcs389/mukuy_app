import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) {}

  post(serviceName: string, data: any) {
    const url = environment.apiUrl + serviceName;
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const options = { headers: headers };
    return this.http.post(url, JSON.stringify(data), options);
  }

  postFormData(serviceName: string, data: any) {
    const url = environment.apiUrl + serviceName;
    const headers = new HttpHeaders();
    const options = { headers: headers };
    return this.http.post(url, data, options);
  }
}
