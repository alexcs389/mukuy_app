import {Component, ElementRef, Inject, NgZone, OnInit, ViewChild} from '@angular/core';
import {CameraResultType, CameraSource, Plugins} from '@capacitor/core';
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {NativeGeocoder} from '@ionic-native/native-geocoder/ngx';
import {StorageService} from '../../services/storage.service';
import {ToastService} from '../../services/toast.service';
import {HttpService} from '../../services/http.service';
import { Router } from '@angular/router';

declare var google;

@Component({
  selector: 'app-messages',
  templateUrl: './messages.page.html',
  styleUrls: ['./messages.page.scss'],
})
export class MessagesPage implements OnInit {

  image: SafeResourceUrl;
  imageReport: any;

  @ViewChild('map', {static: false})
  mapElement: ElementRef;
  map: any;
  address:string;
  lat: string;
  long: string;
  autocomplete: { input: string; };
  autocompleteItems: any[];
  location: any;
  placeid: any;
  GoogleAutocomplete: any;

  marker: any;

  report = {
    title: '',
    description: '',
    latitude: 0,
    longitude: 0,
    user_id: 0
  }

  constructor(private sanitizer: DomSanitizer,
              private geolocation: Geolocation,
              private nativeGeocoder: NativeGeocoder,
              @Inject(NgZone) private zone: NgZone,
              private storageService: StorageService,
              private toastService: ToastService,
              private httpService: HttpService,
              private router: Router) {
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.autocomplete = { input: '' };
    this.autocompleteItems = [];
  }

  ngOnInit() {
    this.storageService.get('userData').then((res: any) => {
      this.report.user_id = res.id;
    });
    this.loadMap();
  }

  saveReport() {
    if (!this.validateData()) {
      this.toastService.presentToast('Por favor llene todos los campos!');
      return;
    }

    console.log(this.report.latitude);
    console.log(this.report.longitude);

    let formData = new FormData();
    formData.append('title', this.report.title);
    formData.append('description', this.report.description);
    formData.append('latitude', this.report.latitude.toString());
    formData.append('longitude', this.report.longitude.toString());
    formData.append('user_id', this.report.user_id.toString());
    formData.append('image', this.imageReport);

    this.httpService.postFormData('v1/reports', formData).subscribe((res: any) => {
      this.toastService.presentToast('El reporte se ha creado correctamente');
      setTimeout(() => this.router.navigate(['home/notifications']), 3000);
    }, (res: any) => {
      if (res.status == 422) {
        this.toastService.presentToast('Por favor llene todos los campos!');
      } else {
        this.toastService.presentToast('Ocurrió un problema inesperado');
      }
    });
  }

  validateData() {
    return this.report.title && this.report.description && this.imageReport &&
        this.report.latitude != 0 && this.report.longitude != 0
  }

  async takePicture() {
    const image = await Plugins.Camera.getPhoto({
      quality: 100,
      allowEditing: false,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera
    });
    this.image = this.sanitizer.bypassSecurityTrustResourceUrl(image && (image.dataUrl));

    const blob = this.DataURIToBlob(image.dataUrl);
    this.imageReport = new File([blob], 'image');
  }

  DataURIToBlob(dataURI: string) {
    const splitDataURI = dataURI.split(',')
    const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1])
    const mimeString = splitDataURI[0].split(':')[1].split(';')[0]

    const ia = new Uint8Array(byteString.length)
    for (let i = 0; i < byteString.length; i++)
      ia[i] = byteString.charCodeAt(i)

    return new Blob([ia], { type: mimeString })
  }

  loadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {
      let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.hybrid
      };

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.addMarker(latLng);
      this.map.addListener('click', (event) => this.addMarker(event.latLng));

    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  addMarker(location) {
    if (this.marker) {
      this.marker.setPosition(location);
    } else {
      this.marker = new google.maps.Marker({
        position: location,
        map: this.map,
      });
    }
    this.report.latitude = location.lat();
    this.report.longitude = location.lng();
  }
}
