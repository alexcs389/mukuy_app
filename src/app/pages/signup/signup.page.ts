import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthConstants } from './../../config/auth-constants';
import { AuthService } from './../../services/auth.service';
import { StorageService } from './../../services/storage.service';
import { ToastService } from './../../services/toast.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss']
})
export class SignupPage implements OnInit {
  postData = {
    name: '',
    lastname: '',
    username: '',
    email: '',
    password: '',
    phone_number: '',
    dependence: '',
    job: '',
  };

  constructor(
    private authService: AuthService,
    private toastService: ToastService,
    private storageService: StorageService,
    private router: Router
  ) {}

  ngOnInit() {}

  validateInputs() {
    let username = this.postData.username.trim();
    let password = this.postData.password.trim();
    let email = this.postData.email.trim();
    let name = this.postData.name.trim();
    return (
      this.postData.name &&
      this.postData.username &&
      this.postData.password &&
      this.postData.email &&
      email.length > 0 &&
      username.length > 0 &&
      email.length > 0 &&
      password.length > 0
    );
  }

  signupAction() {
    if (this.validateInputs()) {
      let data = {user: this.postData};

      this.authService.signup(data).subscribe(
        (res: any) => {
          // console.log(res)
          this.storageService
              .store(AuthConstants.AUTH, res.user)
              .then(res => {
                this.router.navigate(['home']);
              });
        },
        (error: any) => {
          // console.log(error);
          this.toastService.presentToast('Network Issue.');
        }
      );
    } else {
      this.toastService.presentToast(
        'Please enter name, email, username or password.'
      );
    }
  }
}
